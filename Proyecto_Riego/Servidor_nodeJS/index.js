//////////////////////WEBSERVER///////////////////

const express = require('express'); 
const app = express();
app.use(express.static(__dirname + '/public/'));

var succesServer = function () { 
    console.log('Servidor web escuchando en el puerto 3000'); 
}

app.get('/', function (req, res) { 
    console.log("entraron"); 
    res.sendFile(__dirname + '/public/index.html');
});

const server = app.listen('3000', succesServer);

//////////////////////SERIAL PORT///////////////////
const SerialPort = require('serialport'); 
const Readline = require('@serialport/parser-readline');

const configSerialPort = {
    baudRate: 9600 
}

const port = SerialPort('COM4', configSerialPort); 
const parser = port.pipe(new Readline({ delimiter: '~' }));
port.on('open', () => console.log('open'));
port.on('err', (err) => { 
    console.log(err); 
});

parser.on('data', function (data) { 
    var bits = data; 
    var dataSplit = bits.split('/'); 
    io.emit('prueba', dataSplit);
    console.log(dataSplit[1]); 
});

////////////////////////////////////// websocket /////////////////////

const io = require('socket.io').listen(server);

io.on('connection', function (socket) { 
    console.log('Un usuario conectado, información del usuario: ', socket
);
    socket.on('disconnect', function () { 
        console.log('user disconnected');
    });
    socket.on('arduino:data', function (dato) { 
        console.log(dato); 
    });
});